//Não pode usar espaços!
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
//Struct do Elemento da PilhaLista.
typedef struct Elemento {
        char dado;
        struct Elemento * proximo;
}Elemento;

//Struct da Pilha com ponteiro pra struct de Elementos
typedef struct Pilha {
        struct Elemento *inicio;
}Pilha;



//Pilha vazia? 1 SIM / 0 NÃO
int vazia(Pilha *p) {
        if(p->inicio == NULL)
                return 1;
        else
                return 0;
}

//empilha dados na PilhaLista.
void empilha(char valor, Pilha *p) {
        int i=0;
                Elemento * novoElemento = (Elemento *) malloc(sizeof(Elemento));
                novoElemento->dado = valor;
                
                if(p->inicio==NULL){
                        novoElemento->proximo = NULL;
                }else{
                        novoElemento->proximo = p->inicio;
                }
                p->inicio = novoElemento;
}

//desempilha dados da PilhaLista.
char desempilha(Pilha * p) {
        char temp =' ';
        if (p->inicio == NULL) {
                printf("*****ERRO! Pilha Vazia!*****\n");
                return temp;
        } else{
                temp = p->inicio->dado;
                Elemento * removido = p->inicio;
                p->inicio = p->inicio->proximo;
                free(removido);
                return temp;
        }
}

//Ler a expreção usando string.
void lerExpressao(char *dado) {
        printf("Digite a expreção completa.\n");
        scanf("%s", dado);
}
//Transforma char em int pela ASCII
int ascii(char dado) {
        return (int)dado - 48;
}

//Se 1 É NÚMERO. Se 0 NÃO É NÚMERO.
int eNumero(char dado) {
        //if(dado == '0' || dado == '1' || dado == '2' ||  dado == '3' ||  dado == '4' ||  dado == '5' ||  dado == '6' ||  dado == '7' ||  dado == '8' ||  dado == '9')
        if((int) dado > 47 && (int) dado < 58)
                return 1;
        else
                return 0;      
}

int ePreferencia(char dado) {
        if(dado == '(' || dado == ')')
                return 1;
        else
                return 0;
}
int eOperador(char dado) {
        if(dado == '+' || dado == '*' || dado == '/' || dado == '-')
                return 1;
        else
                return 0;
}

int prioridadeOperador(char dado) {

        if(dado == '+' || dado == '-')
                return 1;
        else if(dado == '*' || dado == '/') 
                return 2;
        else 
                return 0;
}

int validaExpressao(char *dado) {
        int i;
        int conta=0;
        int pref1=0, pref2=0;
        int fim = strlen(dado);

        for(i=0;i<fim;i++){
                if(!eNumero(dado[i]) && !eOperador(dado[i]) && !ePreferencia(dado[i])) {
                        printf("ERRO 0! EXPREÇÃO INVALIDA\n");
                        exit(0);
                } else if(dado[i] == '(') {
                                conta++;
                                pref1 = i;
                } else if(dado[i] == ')') {
                                conta--;
                                pref2 = i;
                }
        }
        if(pref1 > pref2) {
                printf("ERRO 1! EXPREÇÃO INVALIDA\n");
                exit(0);
        }else if(conta != 0) {
                printf("ERRO 2! EXPREÇÃO INVALIDA\n");
                exit(0);
        }
        return 1;
}

char * transformar(char *dado) {
        Pilha * novaPilha = (Pilha *) malloc(sizeof(Pilha));
        novaPilha->inicio = NULL;
        int i;
        int j=0;
        char * pos = (char *)malloc(sizeof(char)*strlen(dado));
        int tamanho = strlen(dado);
        for(i=0;i<strlen(dado);i++) {
                if(eNumero(dado[i])) {
                        while(!eOperador(dado[i]) && !ePreferencia(dado[i])&& dado[i]!='\0') {
                                pos[j++] = dado[i++];
                        }
                        pos[j++]=',';
                        i--;
                }
                else if(eOperador(dado[i])) {
                        if(vazia(novaPilha) || prioridadeOperador(novaPilha->inicio->dado) < prioridadeOperador(dado[i])){
                                empilha(dado[i], novaPilha);
                        }
                        else {
                                pos[j++] = desempilha(novaPilha);
                                empilha(dado[i], novaPilha);
                        }
                }
                else if(dado[i] == '(') {
                        empilha(dado[i], novaPilha);
                } else {
                        while(novaPilha->inicio->dado != '('){
                                pos[j++] = desempilha(novaPilha);
                        }
                        desempilha(novaPilha);
                }
        }
        while(!vazia(novaPilha)){
                pos[j++] = desempilha(novaPilha);
        }

        pos[j] = '\0';
        return pos;
}

int avaliaExpressao(char *dado) {
        Pilha * novaPilha = (Pilha *) malloc(sizeof(Pilha));
        int i, j;
        int calcula=0;
        int total;
        for(i=0;i<strlen(dado);i++) {
                if(eNumero(dado[i])) {
                        printf("n\n");
                        empilha(dado[i], novaPilha);
                } else if (eOperador(dado[i])) {
                        printf("o\n");
                        while(!vazia(novaPilha)){
                                calcula = (int) desempilha(novaPilha);
                                //printf("Calcula: %d\n", calcula);
                                calcula = ascii(calcula);
                                total = calcula;
                                switch(dado[i]) {
                                        case '+' :
                                                total += calcula;
                                                break;
                                        case '-' :
                                                total -= calcula;
                                                break;
                                        case '*' :
                                                total *= calcula;
                                        case '/' :
                                                total /= calcula;
                                }
                                empilha(total, novaPilha);
                                printf("Total: %d\n", total);
                        }
                }
        }
        return desempilha(novaPilha);
}




int main() {    

        Pilha * p = (Pilha *)malloc(sizeof(Pilha));
        char dado[100];

        lerExpressao(dado);
        if(validaExpressao(dado))
                printf("%s\n", dado);

        char *coisa = transformar(dado);
        printf("%s\n", coisa);
               
        //avaliaExpressao(coisa);

        return 0;
}
/*
2*(3+4)/5
(1+2)*3+(4+5)/(6+7)+8)/9
((1+2)*3+(4+5)/(6+7)+8)/9
*/